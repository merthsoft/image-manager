﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using FolderSelect;

namespace Image_Manager {
	public partial class ImageManager : Form {
		class ImageFile {
			private Bitmap image;
			public Bitmap Image {
				get {
					if (image == null) {
						image = (Bitmap)Bitmap.FromFile(File.FullName);
					}
					return image;
				}
			}

			public FileInfo File { get; set; }
			public bool MarkedForDeletion { get; set; }

			public ImageFile(string fileName) {
				MarkedForDeletion = false;
				File = new FileInfo(fileName);

				if (!File.Exists) {
					throw new FileNotFoundException(string.Format("Could not find {0}.", fileName), fileName);
				}

			}

			public void DisposeOfImage() {
				if (image != null) {
					image.Dispose();
					image = null;
				}
			}
		}

		List<ImageFile> images = new List<ImageFile>();
		int currentImageIndex = 0;
		int previousImageIndex = -1;
		ImageFile currentImage { get { return images[currentImageIndex]; } }

		private void toggleImageDeletion() {
			currentImage.MarkedForDeletion = !currentImage.MarkedForDeletion;
			setLabel();
		}

		private void setLabel() {
			markedForDeletionLabel.Text = !currentImage.MarkedForDeletion ? "Not marked for deletion." : "Marked for deletion.";
		}

		private void openImages(string[] files) {
			foreach (string file in files) {
				try {
					ImageFile imageFile = new ImageFile(file);
					if (!(new[] { ".png", ".jpg", ".jpeg", ".bmp", ".gif" }.Contains(imageFile.File.Extension))) {
						continue;
					}
					images.Add(imageFile);
				} catch (Exception e) {
					MessageBox.Show(string.Format("{0} could not be loaded:{1}{2}", new FileInfo(file).Name, Environment.NewLine, e), "Failed to load file.");
				}
			}

			imageScrollBar.Maximum = images.Count - 1;
			setCurrentImage(0);

			markImageForDeletionToolStripMenuItem.Enabled = true;
		}

		private void setCurrentImage(int index) {
			if (index < 0 || index > images.Count) {
				return;
			}
			imageScrollBar.Value = index;
			previousImageIndex = currentImageIndex;
			currentImageIndex = index;
			imageBox.Invalidate();
			setLabel();

			if (previousImageIndex != currentImageIndex && previousImageIndex >= 0 && previousImageIndex < images.Count) {
				images[previousImageIndex].DisposeOfImage();
			}
		}

		public ImageManager() {
			InitializeComponent();
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e) {
			using (OpenFileDialog ofd = new OpenFileDialog()) {
				ofd.Multiselect = true;
				ofd.AddFilter("Image files", "*.png", "*.jpg", "*.jpeg", "*.bmp", "*.gif");

				if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) {
					return;
				}

				openImages(ofd.FileNames);
			}			
		}

		private void openFolderToolStripMenuItem_Click(object sender, EventArgs e) {
			FolderSelectDialog fsd = new FolderSelectDialog();
			fsd.Title = "Select image folder.";

			if (!fsd.ShowDialog()) {
				return;
			}

			openImages(Directory.EnumerateFiles(fsd.FileName).ToArray());
		}

		private void markImageForDeletionToolStripMenuItem_Click(object sender, EventArgs e) {
			toggleImageDeletion();
		}

		private void imageScrollBar_ValueChanged(object sender, EventArgs e) {
			setCurrentImage(imageScrollBar.Value);
		}

		private void imageBox_Paint(object sender, PaintEventArgs e) {
			if (images.Count == 0) { return; }
			e.Graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;
			try {
				Bitmap image = currentImage.Image;
				int height = e.ClipRectangle.Height;
				int width = (int)(((float)height / (float)image.Height) * image.Width);
				e.Graphics.DrawImage(image, e.ClipRectangle.Width / 2 - width / 2, 0, width, height);
			} catch (Exception ex) {
				MessageBox.Show(string.Format("{0} could not be loaded:{1}{2}", currentImage.File.Name, Environment.NewLine, ex), "Failed to load file.");
			}
		}

		private void deleteMarkedFilesToolStripMenuItem_Click(object sender, EventArgs e) {
			foreach (ImageFile image in images) {
				if (image.MarkedForDeletion) {
					try {
						image.DisposeOfImage();
						image.File.Delete();
					} catch (Exception ex) {
						MessageBox.Show(string.Format("{0} could not be deleted.{1}{2}", currentImage.File.Name, Environment.NewLine, ex), "Failed to delete file.");
					}
				}
			}
			images.RemoveAll(i => i.MarkedForDeletion);
			imageScrollBar.Maximum = images.Count - 1;
			setCurrentImage(0);
		}

		private void imageBox_Resize(object sender, EventArgs e) {
			imageBox.Invalidate();
		}
	}
}
