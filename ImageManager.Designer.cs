﻿namespace Image_Manager {
	partial class ImageManager {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageManager));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.markImageForDeletionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.imageScrollBar = new System.Windows.Forms.HScrollBar();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.markedForDeletionLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.imageBox = new System.Windows.Forms.PictureBox();
			this.deleteMarkedFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.menuStrip1.SuspendLayout();
			this.statusStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imageBox)).BeginInit();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(544, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.openFolderToolStripMenuItem,
            this.toolStripSeparator2,
            this.deleteMarkedFilesToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
			this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// openFolderToolStripMenuItem
			// 
			this.openFolderToolStripMenuItem.Name = "openFolderToolStripMenuItem";
			this.openFolderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.O)));
			this.openFolderToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
			this.openFolderToolStripMenuItem.Text = "Open Folder";
			this.openFolderToolStripMenuItem.Click += new System.EventHandler(this.openFolderToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(211, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markImageForDeletionToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "&Edit";
			// 
			// markImageForDeletionToolStripMenuItem
			// 
			this.markImageForDeletionToolStripMenuItem.Enabled = false;
			this.markImageForDeletionToolStripMenuItem.Name = "markImageForDeletionToolStripMenuItem";
			this.markImageForDeletionToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
			this.markImageForDeletionToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
			this.markImageForDeletionToolStripMenuItem.Text = "Mark Image for Deletion";
			this.markImageForDeletionToolStripMenuItem.Click += new System.EventHandler(this.markImageForDeletionToolStripMenuItem_Click);
			// 
			// imageScrollBar
			// 
			this.imageScrollBar.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.imageScrollBar.LargeChange = 1;
			this.imageScrollBar.Location = new System.Drawing.Point(0, 352);
			this.imageScrollBar.Maximum = 0;
			this.imageScrollBar.Name = "imageScrollBar";
			this.imageScrollBar.Size = new System.Drawing.Size(544, 17);
			this.imageScrollBar.TabIndex = 1;
			this.imageScrollBar.ValueChanged += new System.EventHandler(this.imageScrollBar_ValueChanged);
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markedForDeletionLabel});
			this.statusStrip.Location = new System.Drawing.Point(0, 369);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(544, 22);
			this.statusStrip.TabIndex = 2;
			this.statusStrip.Text = "statusStrip1";
			// 
			// markedForDeletionLabel
			// 
			this.markedForDeletionLabel.Name = "markedForDeletionLabel";
			this.markedForDeletionLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// imageBox
			// 
			this.imageBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.imageBox.Location = new System.Drawing.Point(0, 24);
			this.imageBox.Name = "imageBox";
			this.imageBox.Size = new System.Drawing.Size(544, 328);
			this.imageBox.TabIndex = 3;
			this.imageBox.TabStop = false;
			this.imageBox.Paint += new System.Windows.Forms.PaintEventHandler(this.imageBox_Paint);
			this.imageBox.Resize += new System.EventHandler(this.imageBox_Resize);
			// 
			// deleteMarkedFilesToolStripMenuItem
			// 
			this.deleteMarkedFilesToolStripMenuItem.Name = "deleteMarkedFilesToolStripMenuItem";
			this.deleteMarkedFilesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.Delete)));
			this.deleteMarkedFilesToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
			this.deleteMarkedFilesToolStripMenuItem.Text = "Delete Marked Files";
			this.deleteMarkedFilesToolStripMenuItem.Click += new System.EventHandler(this.deleteMarkedFilesToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(256, 6);
			// 
			// ImageManager
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(544, 391);
			this.Controls.Add(this.imageBox);
			this.Controls.Add(this.imageScrollBar);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "ImageManager";
			this.Text = "Image Manager";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.imageBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openFolderToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem markImageForDeletionToolStripMenuItem;
		private System.Windows.Forms.HScrollBar imageScrollBar;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel markedForDeletionLabel;
		private System.Windows.Forms.PictureBox imageBox;
		private System.Windows.Forms.ToolStripMenuItem deleteMarkedFilesToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
	}
}

