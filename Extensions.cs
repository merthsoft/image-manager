﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Image_Manager {
	static class Extensions {
		/// <summary>
		/// Adds a filter to the FileDialog.
		/// </summary>
		/// <param name="text">The text to display.</param>
		/// <param name="ext">The extensions.</param>
		public static void AddFilter(this FileDialog ofd, string text, params string[] ext) {
			StringBuilder compiledExt = new StringBuilder(6 * ext.Length);
			for (int i = 0; i < ext.Length; i++) {
				compiledExt.AppendFormat("{1}{0}", ext[i], i == 0 ? "" : ";");
			}
			ofd.Filter = string.Concat(ofd.Filter, ofd.Filter != "" ? "|" : "", text, "|", compiledExt.ToString());
		}
	}
}
